# RPi Charge Control Socket Server

## General

This application is a socket server that receives info from a laptop about its charge status
and passes that info to a Raspberry Pi controlling the charging.

## Installation

npm install

## Starting

npm run start
